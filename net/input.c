#include "ns.h"

extern union Nsipc nsipcbuf;

void
input(envid_t ns_envid)
{
	binaryname = "ns_input";

	// LAB 6: Your code here:
	// 	- read a packet from the device driver
	//	- send it to the network server
	// Hint: When you IPC a page to the network server, it will be
	// reading from it for a while, so don't immediately receive
	// another packet in to the same physical page.

    struct jif_pkt* pkt =(struct jif_pkt*) malloc(3000);
    while(1){

        driver_recv_packet( (void*) pkt);
        //cprintf("Packet length:%d and ns_envid: %x \n", pkt->jp_len,ns_envid);
        nsipcbuf.pkt.jp_len = pkt->jp_len;
        memcpy(&nsipcbuf.pkt.jp_data,pkt->jp_data,pkt->jp_len);

        ipc_send(ns_envid,NSREQ_INPUT,&nsipcbuf,PTE_USER);
        //    - send the packet to the network server..
        sys_yield();
    }
}
