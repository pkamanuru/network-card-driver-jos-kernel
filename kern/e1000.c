#include <kern/e1000.h>
#include <kern/pmap.h>
#include <inc/string.h>

//Local E1000 Registers divide by 4 for use as uint32_t[] indices

#define STATUS_REG 0x08/4
#define E1000_TDBAL    0x03800/4  /* TX Descriptor Base Address Low - RW */
#define E1000_TDBAH    0x03804/4  /* TX Descriptor Base Address High - RW */
#define E1000_TDLEN    0x03808/4  /* TX Descriptor Length - RW */
#define E1000_TDH      0x03810/4  /* TX Descriptor Head - RW */
#define E1000_TDT      0x03818/4  /* TX Descripotr Tail - RW */
#define E1000_TCTL     0x00400/4  /* TX Control - RW */
#define E1000_TCTL_EN     0x00000002    /* enable tx */
#define E1000_TCTL_PSP    0x00000008   /* pad short packets */
#define E1000_TCTL_CT     0x00000ff0    /* collision threshold */
#define TCTL_COLD 0x00040000
#define E1000_TIPG     0x00410/4  /* TX Inter-packet gap -RW */
#define E1000_RA       0x05400/4  /* Receive Address - RW Array */
#define E1000_MTA      0x05200/4  /* Multicast Table Array - RW Array */
#define E1000_IMC      0x000D8/4  /* Interrupt Mask Clear - WO */
#define E1000_IMS      0x000D0/4  /* Interrupt Mask Set - RW */
#define E1000_RCTL     0x00100/4  /* RX Control - RW */
#define E1000_RDBAL    0x02800/4  /* RX Descriptor Base Address Low - RW */
#define E1000_RDBAH    0x02804/4  /* RX Descriptor Base Address High - RW */
#define E1000_RDLEN    0x02808/4  /* RX Descriptor Length - RW */
#define E1000_RDH      0x02810/4  /* RX Descriptor Head - RW */
#define E1000_RDT      0x02818/4  /* RX Descriptor Tail - RW */

#define E1000_TXD_CMD_EOP 	 0x01
#define E1000_TXD_CMD_RS     0x08 /* Report Status */
#define E1000_TXD_STAT_DD    0x01 /* Descriptor Done */

#define E1000_RXD_STAT_DD       0x01 
#define E1000_RXD_STAT_EOP      0x02    /* End of Packet */

#define LOW32(addr) ( addr & 0x00000000FFFFFFFF )
#define HIGH32(addr) ( addr & 0xFFFFFFFF00000000 )

#define TRANS_DESC_LENGTH 64
#define RECV_DESC_LENGTH 256

#define TRANS_DESC_SIZE 16
#define RECV_DESC_SIZE 16

#define ETHERNET_PACKET_SIZE 1518
#define RECV_BUFFER_SIZE 2048



struct tx_desc
{
	uint64_t addr;
	uint16_t length;
	uint8_t cso;
	uint8_t cmd;
	uint8_t status;
	uint8_t css;
	uint16_t special;
};

struct rx_desc
{
	uint64_t addr;
	uint16_t length;
	uint16_t checksum;
	uint8_t status;
	uint8_t errors;
	uint16_t special;
};

//We are not ensuring this as 16 byte aligned

struct tx_desc trans_desc_list[TRANS_DESC_LENGTH];
struct rx_desc recv_desc_list[RECV_DESC_LENGTH];

char recv_desc_buffer[RECV_DESC_LENGTH][RECV_BUFFER_SIZE];
char trans_desc_buffer[TRANS_DESC_LENGTH][ETHERNET_PACKET_SIZE];

__volatile__ uint32_t *e1000_mmio;

uint32_t e1000r(int index){
	return e1000_mmio[index];
}

static void e1000w( int index, int value){
	e1000_mmio[index] = value;
}

int driver_pci_init(struct pci_func *f)
{
	if ( f == NULL )
		return 0;

	pci_func_enable(f);
	e1000_mmio = mmio_map_region( f->reg_base[0], f->reg_size[0] );
	int i;
	for ( i=0; i<TRANS_DESC_LENGTH; i++ ){
		trans_desc_list[i].addr = PADDR( trans_desc_buffer[i] );
		trans_desc_list[i].cmd = E1000_TXD_CMD_RS; // RS is set to 1.
		trans_desc_list[i].status = E1000_TXD_STAT_DD; // Forcing 'DD' bit to be '1' for the first iteration.
	}

	physaddr_t trans_list_baseaddr = PADDR( trans_desc_list );
	physaddr_t recv_list_baseaddr = PADDR( recv_desc_list );
	if ( trans_list_baseaddr % 16 != 0 )
		panic("tran_list_baseaddr not 16 byte aligned %lx\n", trans_list_baseaddr);
	if ( recv_list_baseaddr % 16 != 0 )
		panic("recv_list_baseaddr not 16 byte aligned %lx\n", trans_list_baseaddr);

	//Test code for STATUS REG
	//cprintf("status reg 0x%x\n", e1000_mmio[STATUS_REG] );

	//Test code for Low and High
	//cprintf("trans_list_baseaddr:%llx  low:%x high:%x\n", trans_list_baseaddr,LOW32(trans_list_baseaddr),HIGH32(trans_list_baseaddr) );

	e1000w( E1000_TDBAL, LOW32(trans_list_baseaddr) );
	e1000w( E1000_TDBAH, HIGH32(trans_list_baseaddr) );
	e1000w( E1000_TDLEN, TRANS_DESC_SIZE*TRANS_DESC_LENGTH);
	e1000w( E1000_TDH, 0);
	e1000w( E1000_TDT, 0);
	uint32_t before = e1000r( E1000_TCTL );
	uint32_t val = e1000r( E1000_TCTL );

	//Set the Enable (TCTL.EN) bit to 1b for normal operation
	val = val | E1000_TCTL_EN;

	//Set the Pad Short Packets (TCTL.PSP) bit to 1b.
	val = val | E1000_TCTL_PSP;

	//Configure the Collision Threshold (TCTL.CT) to the desired value
	val = val | E1000_TCTL_CT;

	//Full duplex 64 byte time (40h) TCTL.COLD
	val = val | TCTL_COLD;
	/*
	  IPGT:
	  1111 1111 1111 1111 1111 1100 0000 1010
	  IPGR1:
	  1111 1111 1111 0000 0001 0011 1111 1111

	  IPGR2:
	  1110 0000 1100 1111 1111 1111 1111 1111
	*/

	e1000w( E1000_TCTL, val );
	val = e1000r( E1000_TIPG );
	val = val & 0xFFFFFC0A;  // IPGT = 10 i.e. 9:0 = 0xA;
	val = val & 0XFFF013FF;  // IPGR1 = 8 i.e 19:10 - 0x8;
	val = val & 0xE0CFFFFF;  // IPGR2 = 12 i.e 2/3*IPGR1
	val = val & 0x1FFFFFFF;  // Reserved = 0
	e1000w(E1000_TIPG,val);
/*
  Program RAL, RAH with Mac Address
  Initialize MTA to Zero
  Program IMS REgisters to enable to any interrupt ( Disabling it by writing 0 to IMC )
  Recieve Descriptor list 16 byte aligned boundary analagous to Transmit Descriptor list
  Setup RDBAL, RDBAH for Recieve Descriptor list base address ( physical address )
  RDLEN - 128 Byte aligned multiple of eight ( 64 in our case )
  Recieve Head and Tail are init to zero after power on
  Head -> 0, Tail -> RDLEN - 1

  RCTL Values

  ( Make sure broadcast is not required)
  RDTR( Cannot have delay time setup, Avoid this )
*/

	for ( i=0; i<RECV_DESC_LENGTH; i++ ){
		recv_desc_list[i].addr = PADDR( recv_desc_buffer[i] );
		recv_desc_list[i].length = 0;
		recv_desc_list[i].checksum = 0;
		recv_desc_list[i].status = 0;
		recv_desc_list[i].errors = 0;
		recv_desc_list[i].special = 0;
	}	

	// Commented for testing. Might have to uncomment.
	for ( i=0; i<128; i++ ){
		e1000w(E1000_MTA+i, 0);
	}

	uint32_t imc = e1000r(E1000_IMC);
	//All interrupts Disabled 32-16 15{Interrupts Clear Flags}0
	/*imc &= 0xFFFF0000;
	e1000w( E1000_IMC, imc);*/
	e1000w( E1000_RDBAL, LOW32(recv_list_baseaddr) );
	e1000w( E1000_RDBAH, HIGH32(recv_list_baseaddr) );
	e1000w( E1000_RDLEN, RECV_DESC_SIZE*RECV_DESC_LENGTH );
	e1000w( E1000_RDH, 0);
	e1000w( E1000_RDT, RECV_DESC_LENGTH-1);

	//Configuring the Recieve Transmission
	//e1000w(E1000_RA, 0x52540012);
	//e1000w(E1000_RA+1, 0x80003456);
	e1000w(E1000_RA, 0x12005452);// 52540012);
	e1000w(E1000_RA+1, 0x80005634);	
	uint32_t rctl = e1000r(E1000_RCTL);
	//Enable Bit set 10 1
	//31-28, 27-24, 23-20, 19-16, 15-12, 11-8, 7-4, 3-0
	//EN Bit  1:1 1b 0010
	rctl |= 0x00000002;
	//LBM Bit 7:6 00b LPE Bit 5:5 0b 0001 = 1
	rctl &= 0xFFFFFF1F;
	//BAM Bit 15:15 0b MO Bit 13:12 00b 0100 = 4
	rctl &= 0xFFFF4FFF;
	//BSIZE Bit 17:16 00b ( 2048 Bytes closest to Ethernet Frame Size and fits it )
	//1100 = C
	rctl &= 0xFFFCFFFF;
	//BSEX Bit 25:25 0b ( IF this is 1 buffer size multiplied by 16 not needed)
	//1101 = D
	rctl &= 0xFDFFFFFF;
	//SECRC Bit 26:26 1b ( GRade scripts requires it to be stripped )
	//0100 = 4
	rctl |= 0x04000000;
	e1000w( E1000_RCTL, rctl );
	cprintf("\t Done with PCI init... \n");
	return 1; // Why??
}
//TODO

int e1000_transmit_packet(void* buffer,uint16_t size){

	if(size>ETHERNET_PACKET_SIZE){
		return -2; // Too big to fill in an ethernet packet.
	}

	uint32_t tdTail = e1000r(E1000_TDT);
	if(trans_desc_list[tdTail].status&E1000_TXD_STAT_DD){
		trans_desc_list[tdTail].status&= ~(E1000_TXD_STAT_DD); // Clearing DD bit.
		trans_desc_list[tdTail].cmd|=E1000_TXD_CMD_EOP; // Just for testing purposes??

		trans_desc_list[tdTail].cso = 0; // TODO: Change it something sensible.
		trans_desc_list[tdTail].css = 0; // TODO: Change it something sensible.
		trans_desc_list[tdTail].length = (uint16_t) size; // Surely size<1518, i.e. 11 bits.
		memcpy(trans_desc_buffer[tdTail],buffer,size);
		e1000w(E1000_TDT,(tdTail+1)%TRANS_DESC_LENGTH); // Updating tail pointer.
		return 0;
	}
	return -1; // Transmit buffer full, better luck next time.. 
	panic("\t transmit_packet not implemented \n");
}

int e1000_recv_packet(void* buffer){

	uint32_t rdTail = e1000r(E1000_RDT);
	uint32_t size;
	rdTail = (rdTail+1) % RECV_DESC_LENGTH;
	struct recv_pkt* packet = buffer;
	//buffer = (struct recv_pkt *) buffer;
	if(recv_desc_list[rdTail].status&E1000_RXD_STAT_DD){

		size = recv_desc_list[rdTail].length;
		packet->len = size;
		memcpy(packet->data,recv_desc_buffer[rdTail],size);
		e1000w(E1000_TDT,(rdTail+1)%TRANS_DESC_LENGTH); // Updating tail pointer.

		//recv_desc_list[rdTail].status&= ~(E1000_RXD_STAT_DD); // Clearing DD bit.
		//recv_desc_list[rdTail].status&= ~(E1000_RXD_STAT_EOP); // Clearing EOP bit.

		recv_desc_list[rdTail].checksum = 0;
		recv_desc_list[rdTail].status = 0;
		recv_desc_list[rdTail].errors = 0;
		recv_desc_list[rdTail].special = 0;		

		e1000w(E1000_RDT,rdTail);
		return 0;
	}
	return -1; // Recv buffer empty, better luck next time.. 
	panic("\t recv_packet not implemented \n");
}
