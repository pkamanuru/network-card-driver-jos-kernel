//Simple command-line kernel monitor useful for
// controlling the kernel and exploring the system interactively.

#include <inc/stdio.h>
#include <inc/string.h>
#include <inc/memlayout.h>
#include <inc/assert.h>
#include <inc/x86.h>

#include <kern/console.h>
#include <kern/monitor.h>
#include <kern/dwarf.h>
#include <kern/kdebug.h>
#include <kern/dwarf_api.h>
#include <kern/trap.h>

#define CMDBUF_SIZE	80	// enough for one VGA text line


struct Command {
	const char *name;
	const char *desc;
	// return -1 to force monitor to exit
	int (*func)(int argc, char** argv, struct Trapframe* tf);
};

static struct Command commands[] = {
	{ "help", "Display this list of commands", mon_help },
	{ "kerninfo", "Display information about the kernel", mon_kerninfo },
	{ "backtrace", "Displays the trce of the function ", mon_backtrace},
};
#define NCOMMANDS (sizeof(commands)/sizeof(commands[0]))

/***** Implementations of basic kernel monitor commands *****/

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
	int i;

	for (i = 0; i < NCOMMANDS; i++)
		cprintf("%s - %s\n", commands[i].name, commands[i].desc);
	return 0;
}

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
	extern char _start[], entry[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
	cprintf("  _start                  %08x (phys)\n", _start);
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
	cprintf("Kernel executable memory footprint: %dKB\n",
		ROUNDUP(end - entry, 1024) / 1024);
	return 0;
}

int
mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{
	// Your code here.
	uint64_t orig_rbp=read_rbp();
	uint64_t cur_rbp ; //= orig_rbp;
	uint64_t cur_rip;
	uint64_t adrsFromStart,temp; 
	struct Ripdebuginfo info;
        uint64_t i,useAdrs;
	cprintf("Stack backtrace:");
        read_rip(cur_rip);
	debuginfo_rip(cur_rip,&info);
        adrsFromStart = cur_rip - info.rip_fn_addr;
	cprintf("\n rbp %016llx rip %016llx\n    %s:%d: %s+%016llx  args:%d  %d  ",orig_rbp,cur_rip,info.rip_file,info.rip_line,info.rip_fn_name,adrsFromStart,info.rip_fn_narg,info.reg_table.rules[0].dw_offset);;
	while(orig_rbp!=0)
	{
		cur_rbp = read_addr(orig_rbp);
		cur_rip = read_addr(orig_rbp+8);

		debuginfo_rip(cur_rip,&info);
		adrsFromStart = cur_rip - info.rip_fn_addr;
		//cprintf("\n  rbp %016llx  rip %016llx ",orig_rbp,cur_rip);	
		if(cur_rbp!=0)
		cprintf("\n rbp %016llx rip %016llx\n    %s:%d: %s+%016llx  args:%d  ",orig_rbp,cur_rip,info.rip_file,info.rip_line,info.rip_fn_name,adrsFromStart,info.rip_fn_narg);
		//assert(info.rip_fn_narg<=10);
		for(i=0;i<info.rip_fn_narg;i++){
			useAdrs = ( (8*( info.rip_fn_narg-i+2)));//-info.size_fn_arg[i]);
			temp = read_addr(orig_rbp+useAdrs);
			//cprintf("\n useAdrs: %016llx ",useAdrs);
			switch(info.size_fn_arg[i]){
				case 1:
					useAdrs = 0xff00000000000000;
					temp = temp & useAdrs;
					temp = temp >> 56;
					//cprintf("\t Case-1: %016llx ",useAdrs);
					break;
				case 2:
					useAdrs = 0xffff000000000000;
					temp = temp & useAdrs;
					temp = temp >> 48;
					//cprintf("\t Case-2: %016llx ",useAdrs);
					break;
				case 4:
					useAdrs = 0xffffffff00000000;;//( ( (2^64)-1) ^ ((2^32)-1));
					//useAdrs = 0x00000000ffffffff;
					//useAdrs = 0xffffffffffffffff;
					temp = temp & useAdrs;
					temp = temp >> 32;
					//cprintf("\t Case-4: %016llx ",useAdrs);
					break;
				default:
					useAdrs = 0xffffffffffffffff;
					temp = temp & useAdrs;
					//cprintf("\t Case-8: %016llx ",useAdrs);
					break;
			}
			cprintf("%016llx ",temp);	
			//cprintf("%016llx %d %016llx ",temp,info.size_fn_arg[i],useAdrs);
		}
				
		orig_rbp = cur_rbp;
	}
	cprintf("\n");
	return 0;
}



/***** Kernel monitor command interpreter *****/

#define WHITESPACE "\t\r\n "
#define MAXARGS 16

static int
runcmd(char *buf, struct Trapframe *tf)
{
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
		if (*buf == 0)
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
	}
	argv[argc] = 0;

	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < NCOMMANDS; i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
	return 0;
}

void
monitor(struct Trapframe *tf)
{
	char *buf;

	cprintf("Welcome to the JOS kernel monitor!\n");
	cprintf("Type 'help' for a list of commands.\n");

	if (tf != NULL)
		print_trapframe(tf);

	while (1) {
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
				break;
	}
}
