#ifndef JOS_KERN_E1000_H
#define JOS_KERN_E1000_H
#include <kern/pci.h>

int driver_pci_init(struct pci_func *f);
int e1000_transmit_packet(void* buffer,uint16_t size);
int e1000_recv_packet(void* buffer);
struct recv_pkt{ // Same as jif_pkt.
    int len;
    char data[0];
};
#endif	// JOS_KERN_E1000_H
