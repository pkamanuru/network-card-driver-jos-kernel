// implement fork from user space

#include <inc/string.h>
#include <inc/lib.h>

// PTE_COW marks copy-on-write page table entries.
// It is one of the bits explicitly allocated to user processes (PTE_AVAIL).
#define PTE_COW        0x800

// Assembly language pgfault entrypoint defined in lib/pfentry.S.
extern void _pgfault_upcall(void);
//
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
	void *addr = (void *) utf->utf_fault_va;
	uint32_t err = utf->utf_err;
	int r;

	// Check that the faulting access was (1) a write, and (2) to a
	// copy-on-write page.  If not, panic.
	// Hint:
	//   Use the read-only page table mappings at uvpt
	//   (see <inc/memlayout.h>).
	int perm = uvpt[PGNUM(addr)] & 0xFFF;
	if ( ! ( err & FEC_WR && perm & PTE_COW ) )
		panic("unexpected page_fault in pfault\n");
	//cprintf("\t addr: %llx VPN(addr): %llx ",addr,VPN(addr));
	// LAB 4: Your code here.

	// Allocate a new page, map it at a temporary location (PFTEMP),
	// copy the data from the old page to the new page, then move the new
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.
	envid_t myenvid = sys_getenvid();
	r = sys_page_alloc( myenvid, (void*) PFTEMP, PTE_P | PTE_W | PTE_U );
	if ( r < 0 )
		panic("sys_page_alloc failed:%d\n",r);

	addr = ( void *) (VPN(addr)*PGSIZE);
	void *temp_addr = (void *) (PFTEMP );
	memmove( temp_addr, addr, PGSIZE );

	perm = (perm & ~PTE_COW) | PTE_W;
	r = sys_page_map( myenvid, temp_addr, myenvid, addr, perm );
	if ( r < 0 )
		panic("sys_page_map failed\n");
	//panic("pgfault not implemented");
}

//
// Map our virtual page pn (address pn*PGSIZE) into the target envid
// at the same virtual address.  If the page is writable or copy-on-write,
// the new mapping must be created copy-on-write, and then our mapping must be
// marked copy-on-write as well.  (Exercise: Why do we need to mark ours
// copy-on-write again if it was already copy-on-write at the beginning of
// this function?)
//
// Returns: 0 on success, < 0 on error.
// It is also OK to panic on error.
//
static int
duppage(envid_t envid, unsigned pn)
{
    int r;

    //sys_page_map(envid_t srcenvid, void *srcva,envid_t dstenvid, void *dstva, int perm)
    envid_t myenvid = sys_getenvid();
    uintptr_t virt_addr = ( uintptr_t ) pn*PGSIZE;

    int perm = uvpt[pn] & 0xFFF;
    int ret_value;
    if(perm &PTE_SHARE){
        perm = uvpt[pn] & PTE_SYSCALL;
        ret_value = sys_page_map( myenvid, (void*) virt_addr, envid, (void*) virt_addr, perm );
        if ( ret_value < 0 )
            panic("sys_page_map failed in duppage\n");        
    }else if ( perm & PTE_W || perm & PTE_COW ){
    	perm = perm & ~PTE_W;
    	perm = perm | PTE_COW;
        ret_value = sys_page_map( myenvid, (void*) virt_addr, envid, (void*) virt_addr, perm );
    	if ( ret_value < 0 )
    		panic("sys_page_map failed\n");
    	ret_value = sys_page_map( myenvid, (void*) virt_addr, myenvid, (void*) virt_addr, perm );
    	if ( ret_value < 0 )
    		panic("sys_page_map failed\n");
    }else{
    	ret_value = sys_page_map( myenvid, (void*) virt_addr, envid, (void*) virt_addr, perm );
        if ( ret_value < 0 )
            panic("sys_page_map failed in duppage\n");
    }
    return 0;
}


//
// User-level fork with copy-on-write.
// Set up our page fault handler appropriately.
// Create a child.
// Copy our address space and page fault handler setup to the child.
// Then mark the child as runnable and return.
//
// Returns: child's envid to the parent, 0 to the child, < 0 on error.
// It is also OK to panic on error.
//
// Hint:
//   Use uvpd, uvpt, and duppage.
//   Remember to fix "thisenv" in the child process.
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
    // LAB 4: Your code here.
    // Set up our page fault handler appropriately.
    set_pgfault_handler(&pgfault);

    // Create a child.
    envid_t envid;
    envid = sys_exofork();
    if (envid < 0)
        panic("sys_exofork: %e", envid);
    if (envid == 0) {
        // We're the child.
        // The copied value of the global variable 'thisenv'
        // is no longer valid (it refers to the parent!).
        // Fix it and return 0.
        //set_pgfault_handler(&pgfault); // CBLater: and page fault handler setup to the child.
        thisenv = &envs[ENVX(sys_getenvid())];
        return 0;
    }

    //cprintf("\t uv My pgfault_handler is: %llx \n",_pgfault_handler);
    // Copy our address space...
    uint64_t i,j,k,l;
    for(i=0;i<1;i++){ //CBAssumption: Since every VA below UTOP goes to UVMPLE[0].
        if(uvpml4e[i]!=0){
            //cprintf("\t i: %llx uvpml4e[i]: %llx \n",i,uvpml4e[i]);
            for(j=i*NPDPENTRIES;j<(i+1)*NPDPENTRIES;j++){
                if(uvpde[j]!=0){
                    //cprintf("\t i: %llx j: %llx uvpde[j]: %llx \n",i,j,uvpde[j]);
                    for(k=j*NPDENTRIES;k<(j+1)*NPDENTRIES;k++){
                        if(uvpd[k]!=0){
                            //cprintf("\t i: %llx j: %llx k: %llx uvpde[k]: %llx \n",i,j,k,uvpd[k]);
                            for(l=k*NPTENTRIES;l<(k+1)*NPTENTRIES;l++){
                               if(uvpt[l]!=0){
                                //if( (uvpt[l]&PTE_W || uvpt[l]&PTE_COW) && ( (PGSIZE*l) !=(UXSTACKTOP-PGSIZE) && ((PGSIZE*l) !=(USTACKTOP-PGSIZE)) ) ){
                                if( (PGSIZE*l) !=(UXSTACKTOP-PGSIZE)){
                                    //cprintf("\t i: %llx j: %llx k: %llx l: %llx l*PN: %llx uvpt[l]: %llx \n",i,j,k,l,l*PGSIZE,uvpt[l]);
                                    duppage(envid,l);
                                }
                               }
                            }
                        }
                    }
                }
            }
        }
    }
    //panic("\t fork not implemented \n");
    // Pending: Copying User and User exception stack.
    int r;
    // CBAssumption: Since we need to copy, this should be fine.
    if ((r = sys_page_alloc(envid, (void*)UXSTACKTOP-PGSIZE, PTE_P|PTE_U|PTE_W)) < 0)
        panic("sys_page_alloc: %e", r);
    if ((r = sys_page_map(envid, (void*)UXSTACKTOP-PGSIZE, 0, UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
        panic("sys_page_map: %e", r);
    memmove(UTEMP, (void*)UXSTACKTOP-PGSIZE, PGSIZE); // Since fork needs child to have same state as parent, should copy stack?
    if ((r = sys_page_unmap(0, (void*)UTEMP)) < 0)
        panic("sys_page_unmap: %e", r);

    // CBAssumption: Since we need to copy, this should be fine.
    /*if ((r = sys_page_alloc(envid, (void*)USTACKTOP-PGSIZE, PTE_P|PTE_U|PTE_W)) < 0)
        panic("sys_page_alloc: %e", r);
    if ((r = sys_page_map(envid, (void*)USTACKTOP-PGSIZE, 0, UTEMP, PTE_P|PTE_U|PTE_W)) < 0)
        panic("sys_page_map: %e", r);
    memmove(UTEMP, (void*)USTACKTOP-PGSIZE, PGSIZE); // Since fork needs child to have same state as parent, should copy stack?
    if ((r = sys_page_unmap(0, (void*)UTEMP)) < 0)
        panic("sys_page_unmap: %e", r);*/

    //page fault handler setup to the child.
    sys_env_set_pgfault_upcall( envid, _pgfault_upcall );

    //panic("fork not implemented");
    // Start the child environment running
    if ((r = sys_env_set_status(envid, ENV_RUNNABLE)) < 0)
        panic("sys_env_set_status: %e", r);

    return envid;

}

// Challenge!
int
sfork(void)
{
    panic("sfork not implemented");
    return -E_INVAL;
}
