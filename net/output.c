#include "ns.h"

extern union Nsipc nsipcbuf;
void
output(envid_t ns_envid)
{
	binaryname = "ns_output";
	// LAB 6: Your code here:
	// 	- read a packet from the network server
	while(1){
	  ipc_recv( &ns_envid, &nsipcbuf, NULL);
	  struct jif_pkt *pkt = (struct jif_pkt *)&nsipcbuf;
	  //	- send the packet to the device driver
	  //cprintf("Packet length:%d\n", pkt->jp_len);
	  driver_transmit_packet( (void*) pkt->jp_data, (uint16_t) pkt->jp_len);
	}
}
