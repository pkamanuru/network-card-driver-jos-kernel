/* See COPYRIGHT for copyright information. */

#include <inc/x86.h>
#include <inc/error.h>
#include <inc/string.h>
#include <inc/assert.h>

#include <kern/e1000.h>
#include <kern/env.h>
#include <kern/pmap.h>
#include <kern/trap.h>
#include <kern/syscall.h>
#include <kern/console.h>
#include <kern/sched.h>
#include <kern/time.h>
#include <kern/e1000.h>

#define check_page_aligned(x) ( ( (uintptr_t)x & 0xfff ) == 0 )
// Print a string to the system console.
// The string is exactly 'len' characters long.
// Destroys the environment on memory errors.
static void
sys_cputs(const char *s, size_t len)
{
	// Check that the user has permission to read memory [s, s+len).
	// Destroy the environment if not.

	// LAB 3: Your code here.
	user_mem_assert(curenv,(void *)s,len,PTE_U);
	// Print the string supplied by the user.
	cprintf("%.*s", len, s);
}

// Read a character from the system console without blocking.
// Returns the character, or 0 if there is no input waiting.
static int
sys_cgetc(void)
{
	return cons_getc();
}

// Returns the current environment's envid.
static envid_t
sys_getenvid(void)
{
	return curenv->env_id;
}

// Destroy a given environment (possibly the currently running environment).
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
static int
sys_env_destroy(envid_t envid)
{
	int r;
	struct Env *e;

	if ((r = envid2env(envid, &e, 1)) < 0)
		return r;
	if (e == curenv)
		cprintf("[%08x] exiting gracefully\n", curenv->env_id);
	else
		cprintf("[%08x] destroying %08x\n",curenv->env_id,e->env_id);
	env_destroy(e);
	return 0;
}

// Deschedule current environment and pick a different one to run.
static void
sys_yield(void)
{
	sched_yield();
}

// Allocate a new environment.
// Returns envid of new environment, or < 0 on error.  Errors are:
//	-E_NO_FREE_ENV if no free environment is available.
//	-E_NO_MEM on memory exhaustion.
static envid_t
sys_exofork(void)
{
	// Create the new environment with env_alloc(), from kern/env.c.
	// It should be left as env_alloc created it, except that
	// status is set to ENV_NOT_RUNNABLE, and the register set is copied
	// from the current environment -- but tweaked so sys_exofork
	// will appear to return 0.
    struct Env* newenv;
    int r;

    //CBAssumption: Using curenv is fine?

    if((r= env_alloc(&newenv,curenv->env_id)) !=0)
    	return r;
    newenv->env_status = ENV_NOT_RUNNABLE;
    //newenv->env_tf.tf_regs = curenv->env_tf.tf_regs; // CBLater: How to tweak it so sys_exofork returns 0.

    newenv->env_tf = curenv->env_tf;
    newenv->env_tf.tf_regs.reg_rax = 0;
    return newenv->env_id;
	// LAB 4: Your code here.
	//panic("sys_exofork not implemented");
}

// Set envid's env_status to status, which must be ENV_RUNNABLE
// or ENV_NOT_RUNNABLE.
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
//	-E_INVAL if status is not a valid status for an environment.
static int
sys_env_set_status(envid_t envid, int status)
{
	// Hint: Use the 'envid2env' function from kern/env.c to translate an
	// envid to a struct Env.
	// You should set envid2env's third argument to 1, which will
	// check whether the current environment has permission to set
	// envid's status.

	// LAB 4: Your code here.
	struct Env *req_env; 
	if ( status != ENV_RUNNABLE && status != ENV_NOT_RUNNABLE )
		return -E_INVAL;
	int ret = envid2env(envid, &req_env, 1) ;
	if ( ret == 0 )
		req_env->env_status = status;
	return ret;
	//panic("sys_env_set_status not implemented");
}

// Set envid's trap frame to 'tf'.
// tf is modified to make sure that user environments always run at code
// protection level 3 (CPL 3) with interrupts enabled.
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
static int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	// LAB 5: Your code here.
	// Remember to check whether the user has supplied us with a good
	// address!
	if(tf==NULL || (uintptr_t)tf>= UTOP){
		return -E_INVAL;
	}
	struct Env *req_env; 
	int ret = envid2env(envid, &req_env, 1) ;
	if ( ret == 0 )	
		req_env->env_tf = *tf;
	return ret;
	panic("sys_env_set_trapframe not implemented");
}

// Set the page fault upcall for 'envid' by modifying the corresponding struct
// Env's 'env_pgfault_upcall' field.  When 'envid' causes a page fault, the
// kernel will push a fault record onto the exception stack, then branch to
// 'func'.
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
static int
sys_env_set_pgfault_upcall(envid_t envid, void *func)
{
	// LAB 4: Your code here.
	struct Env *reqEnv;
	int ret = envid2env(envid, &reqEnv, 1) ;
	if ( ret == 0 )
		reqEnv->env_pgfault_upcall = func;

	return ret;
}

// Allocate a page of memory and map it at 'va' with permission
// 'perm' in the address space of 'envid'.
// The page's contents are set to 0.
// If a page is already mapped at 'va', that page is unmapped as a
// side effect.
//
// perm -- PTE_U | PTE_P must be set, PTE_AVAIL | PTE_W may or may not be set,
//         but no other bits may be set.  See PTE_SYSCALL in inc/mmu.h.
//
// Return 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
//	-E_INVAL if va >= UTOP, or va is not page-aligned.
//	-E_INVAL if perm is inappropriate (see above).
//	-E_NO_MEM if there's no memory to allocate the new page,
//		or to allocate any necessary page tables.
static int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	// Hint: This function is a wrapper around page_alloc() and
	//   page_insert() from kern/pmap.c.
	//   Most of the new code you write should be to check the
	//   parameters for correctness.
	//   If page_insert() fails, remember to free the page you
	//   allocated!
	// LAB 4: Your code here.
	struct Env *req_env; 
	int r = envid2env(envid,&req_env,1);
	if(r<0)
		return -E_BAD_ENV;

	if ( (uintptr_t) va >= UTOP || !check_page_aligned(va) || ! ( (perm&PTE_U) && (perm&PTE_P) )  )
		return -E_INVAL;

	struct PageInfo* newPage = page_alloc(ALLOC_ZERO);
	if ( newPage == NULL )
		return -E_NO_MEM;
	r = page_insert(req_env->env_pml4e,newPage,va,perm|PTE_P|PTE_U);

	if(r !=0 ){
		page_free(newPage);
		return -E_NO_MEM;
	}

	return r;
	//panic("sys_page_alloc not implemented");
}

// Map the page of memory at 'srcva' in srcenvid's address space
// at 'dstva' in dstenvid's address space with permission 'perm'.
// Perm has the same restrictions as in sys_page_alloc, except
// that it also must not grant write access to a read-only
// page.
//
// Return 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if srcenvid and/or dstenvid doesn't currently exist,
//		or the caller doesn't have permission to change one of them.
//	-E_INVAL if srcva >= UTOP or srcva is not page-aligned,
//		or dstva >= UTOP or dstva is not page-aligned.
//	-E_INVAL is srcva is not mapped in srcenvid's address space.
//	-E_INVAL if perm is inappropriate (see sys_page_alloc).
//	-E_INVAL if (perm & PTE_W), but srcva is read-only in srcenvid's
//		address space.
//	-E_NO_MEM if there's no memory to allocate any necessary page tables.
static int
sys_page_map(envid_t srcenvid, void *srcva,
	     envid_t dstenvid, void *dstva, int perm)
{
	// Hint: This function is a wrapper around page_lookup() and
	//   page_insert() from kern/pmap.c.
	//   Again, most of the new code you write should be to check the
	//   parameters for correctness.
	//   Use the third argument to page_lookup() to
	//   check the current permissions on the page.

	// LAB 4: Your code here.
	struct Env *src_env, *dst_env;
	if ( envid2env( srcenvid, &src_env, 1 ) != 0 || envid2env( dstenvid, &dst_env, 1 ) != 0 )
		return -E_BAD_ENV;

	if ( (uintptr_t) srcva >= UTOP || !check_page_aligned(srcva) || (uintptr_t) dstva >= UTOP ||  ! check_page_aligned(dstva) )
		return -E_INVAL;

	pte_t *pte;
	struct PageInfo *found_page = page_lookup(src_env->env_pml4e, srcva, &pte);

	if ( found_page == NULL || ( perm & PTE_W && !(*pte & PTE_W) ) || !( perm & PTE_U && perm & PTE_P ) )
		return -E_INVAL;
	int ret = page_insert ( dst_env->env_pml4e, found_page, dstva,  perm );
	if ( ret != 0 )
		return -E_NO_MEM;
	return 0;
//	panic("sys_page_map not implemented");
}

// Unmap the page of memory at 'va' in the address space of 'envid'.
// If no page is mapped, the function silently succeeds.
//
// Return 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
//	-E_INVAL if va >= UTOP, or va is not page-aligned.
static int
sys_page_unmap(envid_t envid, void *va)
{
	// Hint: This function is a wrapper around page_remove().
	// LAB 4: Your code here.

	struct Env *req_env;
	if ( envid2env(envid, &req_env, 1) != 0 )
		return -E_BAD_ENV;
	if ( (uintptr_t) va >= UTOP || !check_page_aligned(va) )
		return -E_INVAL;
	page_remove( req_env->env_pml4e, va);
	return 0;

	//panic("sys_page_unmap not implemented");
}

// Try to send 'value' to the target env 'envid'.
// If srcva < UTOP, then also send page currently mapped at 'srcva',
// so that receiver gets a duplicate mapping of the same page.
//
// The send fails with a return value of -E_IPC_NOT_RECV if the
// target is not blocked, waiting for an IPC.
//
// The send also can fail for the other reasons listed below.
//
// Otherwise, the send succeeds, and the target's ipc fields are
// updated as follows:
//    env_ipc_recving is set to 0 to block future sends;
//    env_ipc_from is set to the sending envid;
//    env_ipc_value is set to the 'value' parameter;
//    env_ipc_perm is set to 'perm' if a page was transferred, 0 otherwise.
// The target environment is marked runnable again, returning 0
// from the paused sys_ipc_recv system call.  (Hint: does the
// sys_ipc_recv function ever actually return?)
//
// If the sender wants to send a page but the receiver isn't asking for one,
// then no page mapping is transferred, but no error occurs.
// The ipc only happens when no errors occur.
//
// Returns 0 on success, < 0 on error.
// Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist.
//		(No need to check permissions.)
//	-E_IPC_NOT_RECV if envid is not currently blocked in sys_ipc_recv,
//		or another environment managed to send first.
//	-E_INVAL if srcva < UTOP but srcva is not page-aligned.
//	-E_INVAL if srcva < UTOP and perm is inappropriate
//		(see sys_page_alloc).
//	-E_INVAL if srcva < UTOP but srcva is not mapped in the caller's
//		address space.
//	-E_INVAL if (perm & PTE_W), but srcva is read-only in the
//		current environment's address space.
//	-E_NO_MEM if there's not enough memory to map srcva in envid's
//		address space .
static int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, unsigned perm)
{
	// LAB 4: Your code here.
	//find target env_id
	pte_t *pte;
	struct PageInfo *found_page = page_lookup(curenv->env_pml4e, srcva, &pte);

	struct Env *dst_env;
	if ( envid2env( envid, &dst_env, 0 ) != 0 )
		return -E_BAD_ENV;	

	//Checking source va is mapped in the caller's address space
	if ( srcva != NULL && pte == NULL )
		return -E_INVAL;

	physaddr_t ram_addr = -1;
	if ( srcva != NULL ){
		ram_addr = *pte;	
		//pte is valid so checking for permissions in the page
		if ( (uintptr_t) srcva < UTOP && (!check_page_aligned(srcva)) )
			return -E_INVAL;
		else if ( (uintptr_t) srcva < UTOP &&  ! ( ram_addr & PTE_U || ram_addr & PTE_P )  )
			return -E_INVAL;
		else if ( perm & PTE_W && ! ( (uintptr_t) ram_addr & PTE_W) )
			return -E_INVAL;
	}
	int i=0;

	if ( dst_env->env_ipc_recving != true )
		return -E_IPC_NOT_RECV;
	//Found Environment to send
	dst_env->env_ipc_perm = 0;
	int ret = 0;

	if ( dst_env->env_ipc_dstva != NULL && srcva != NULL && ((uintptr_t)srcva < UTOP )){ 
		ret = page_insert ( dst_env->env_pml4e, found_page, dst_env->env_ipc_dstva,  perm );
		if ( ret != 0 )
			return -E_NO_MEM;
		dst_env->env_ipc_perm = perm;
	} 

	//Complete Success
	dst_env->env_ipc_recving = 0;

	dst_env->env_ipc_value = value;
	dst_env->env_ipc_from = curenv->env_id;
	
	dst_env->env_status = ENV_RUNNABLE;	
	dst_env->env_tf.tf_regs.reg_rax = 0;
	//cprintf("FOUND SHIT-3 dst_env->env_ipc_value: %d ****\n",dst_env->env_ipc_value);
	return 0;

}

// Block until a value is ready.  Record that you want to receive
// using the env_ipc_recving and env_ipc_dstva fields of struct Env,
// mark yourself not runnable, and then give up the CPU.
//
// If 'dstva' is < UTOP, then you are willing to receive a page of data.
// 'dstva' is the virtual address at which the sent page should be mapped.
//
// This function only returns on error, but the system call will eventually
// return 0 on success.
// Return < 0 on error.  Errors are:
//	-E_INVAL if dstva < UTOP but dstva is not page-aligned.
static int
sys_ipc_recv(void *dstva)
{
	// LAB 4: Your code here.
	if (  dstva != NULL &&  ( (uintptr_t) dstva < (uintptr_t) UTOP ) && ( ( (uintptr_t)dstva & 0xFFF )!= 0 ) ){
		return -E_INVAL;
	}

	curenv->env_ipc_recving = true;
	curenv->env_ipc_dstva = dstva;
	curenv->env_status = ENV_NOT_RUNNABLE;
	sched_yield();
	//return 0;
}

// Return the current time.
static int
sys_time_msec(void)
{
	// LAB 6: Your code here.
	return time_msec();
}

static int 
sys_transmit_packet(void* buffer,int size){
	
	user_mem_assert(curenv,(void *)buffer,size,PTE_U);
	return e1000_transmit_packet(buffer,size);
}

static int 
sys_recv_packet(void* buffer){
	
	//user_mem_assert(curenv,(void *)buffer,size,PTE_U);
	return e1000_recv_packet(buffer);
}

// Dispatches to the correct kernel function, passing the arguments.
int64_t
syscall(uint64_t syscallno, uint64_t a1, uint64_t a2, uint64_t a3, uint64_t a4, uint64_t a5)
{
	// Call the function corresponding to the 'syscallno' parameter.
	// Return any appropriate return value.
	// LAB 3: Your code here.

	//cprintf("\t <kern/syscall> syscallno: %llx a1: %llx a2: %llx a3: %llx a4: %llx a5: %llx\n",syscallno,a1,a2,a3,a4,a5);
	//panic("syscall not implemented");
	int64_t i;
	switch (syscallno) {
		case SYS_cputs:
			sys_cputs( (char*) a1, (size_t) a2 );
			return a2;
			break;
		case SYS_cgetc:
			i = sys_cgetc();
			return i;
			break;
		case SYS_getenvid:
			i = sys_getenvid();
			return i;
			break;
		case SYS_env_destroy:
			i = sys_env_destroy((envid_t)a1);
			return i;
			break;
		case SYS_exofork:
			i = sys_exofork();
			return i;
			break;
		case SYS_page_alloc:
			i = sys_page_alloc( (envid_t)a1, (void*)a2, (int) a3);
			return i;
			break;
		case SYS_page_map:
			i = sys_page_map( (envid_t)a1, (void*)a2, (envid_t)a3, (void*)a4, (int)a5);
			return i;
			break;
		case SYS_page_unmap:
			i = sys_page_unmap( (envid_t)a1, (void*)a2 );
			return i;
			break;
		case SYS_env_set_status:
			i = sys_env_set_status( (envid_t)a1, (int)a2 );
			return i;
			break;
		case SYS_env_set_pgfault_upcall:
			i = sys_env_set_pgfault_upcall((envid_t) a1, (void*) a2);
			return i;
			break;
		case SYS_yield:
			sys_yield();
			break;
		case SYS_ipc_try_send:
			i = sys_ipc_try_send( (envid_t) a1, (int) a2, (void*) a3, (int) a4 );
			return i;
			break;
		case SYS_ipc_recv:
			i = sys_ipc_recv( (void*) a1 );
			return i;
			break;
		case SYS_env_set_trapframe:
			i = sys_env_set_trapframe((envid_t) a1,(struct Trapframe*) a2);
			return i;
			break;
		case SYS_transmit_packet:
			i = sys_transmit_packet((void *)a1,(uint16_t)a2);
			return i;
			break;
		case SYS_recv_packet:
			i = sys_recv_packet((void *)a1);
			return i;
			break;			
		case NSYSCALLS:
			cprintf("NSYSCALLS  NON NON ON\n");
			break;
	case SYS_time_msec:
		i = sys_time_msec();
		return i;
		break;
	default:
		return -E_NO_SYS;
	}
	return -E_NO_SYS;
}

