#include <inc/mmu.h>
#include <inc/x86.h>
#include <inc/assert.h>

#include <kern/pmap.h>
#include <kern/trap.h>
#include <kern/console.h>
#include <kern/monitor.h>
#include <kern/env.h>
#include <kern/syscall.h>
#include <kern/sched.h>
#include <kern/kclock.h>
#include <kern/picirq.h>
#include <kern/cpu.h>
#include <kern/spinlock.h>
#include <kern/time.h>

extern uintptr_t gdtdesc_64;
static struct Taskstate ts;
extern struct Segdesc gdt[];
extern long gdt_pd;

/* For debugging, so print_trapframe can distinguish between printing
 * a saved trapframe and printing the current trapframe and print some
 * additional information in the latter case.
 */
static struct Trapframe *last_tf;

/* Interrupt descriptor table.  (Must be built at run time because
 * shifted function addresses can't be represented in relocation records.)
 */
struct Gatedesc idt[256] = { { 0 } };
struct Pseudodesc idt_pd = {0,0};


static const char *trapname(int trapno)
{
	static const char * const excnames[] = {
		"Divide error",
		"Debug",
		"Non-Maskable Interrupt",
		"Breakpoint",
		"Overflow",
		"BOUND Range Exceeded",
		"Invalid Opcode",
		"Device Not Available",
		"Double Fault",
		"Coprocessor Segment Overrun",
		"Invalid TSS",
		"Segment Not Present",
		"Stack Fault",
		"General Protection",
		"Page Fault",
		"(unknown trap)",
		"x87 FPU Floating-Point Error",
		"Alignment Check",
		"Machine-Check",
		"SIMD Floating-Point Exception"
	};

	if (trapno < sizeof(excnames)/sizeof(excnames[0]))
		return excnames[trapno];
	if (trapno == T_SYSCALL)
		return "System call";
	if (trapno >= IRQ_OFFSET && trapno < IRQ_OFFSET + 16)
		return "Hardware Interrupt";
	return "(unknown trap)";
}

void
trap_init(void)
{
	extern struct Segdesc gdt[];

	// LAB 3: Your code here.
    idt_pd.pd_lim = sizeof(idt)-1;
    idt_pd.pd_base = (uint64_t)idt;
  
	//CBAssumptions: Exception 0 to 31, interrupt rest of it.
    //CBLater: IDT 8,15 not defined/reserved.. Should handle it gracefully.
    //CBLater: What about our T_DEFAULT?

    SETGATE( idt[T_DIVIDE] ,1,(uint16_t) GD_KT, (uint64_t)tdivideZero, 0); 
    SETGATE( idt[T_DEBUG] ,1,(uint16_t) GD_KT, (uint64_t)tDebugExceptions, 0); 
    SETGATE( idt[T_NMI] ,1,(uint16_t) GD_KT, (uint64_t)tNonMaskableIntrpt, 0); 
    SETGATE( idt[T_BRKPT] ,0,(uint16_t) GD_KT, (uint64_t)tBreakpoint, 3); 
    SETGATE( idt[T_OFLOW] ,1,(uint16_t) GD_KT, (uint64_t)tOverflow, 0); 
    SETGATE( idt[T_BOUND] ,1,(uint16_t) GD_KT, (uint64_t)tBoundCheck, 0); 
    SETGATE( idt[T_ILLOP] ,1,(uint16_t) GD_KT, (uint64_t)tIllegalOpcode, 0); 
    SETGATE( idt[T_DEVICE] ,1,(uint16_t) GD_KT, (uint64_t)tCoprocNotAvail, 0); 
    SETGATE( idt[T_DBLFLT] ,1,(uint16_t) GD_KT, (uint64_t)tDoubleFault, 0);

    SETGATE( idt[T_TSS] ,1,(uint16_t) GD_KT, (uint64_t)tInvalidTSS, 0); 
    SETGATE( idt[T_SEGNP] ,1,(uint16_t) GD_KT, (uint64_t)tSegmentNotPresent, 0); 
    SETGATE( idt[T_STACK] ,1,(uint16_t) GD_KT, (uint64_t)tStackException, 0); 
    SETGATE( idt[T_GPFLT] ,1,(uint16_t) GD_KT, (uint64_t)tGeneralProtFault, 0); 
    SETGATE( idt[T_PGFLT] ,0,(uint16_t) GD_KT, (uint64_t)tPageFault, 0); 

    SETGATE( idt[T_FPERR] ,1,(uint16_t) GD_KT, (uint64_t)tFloatingPointError, 0); 
    SETGATE( idt[T_ALIGN] ,1,(uint16_t) GD_KT, (uint64_t)tAlignmentCheck, 0); 
    SETGATE( idt[T_MCHK] ,1,(uint16_t) GD_KT, (uint64_t)tMachineCheck, 0); 
    SETGATE( idt[T_SIMDERR] ,1,(uint16_t) GD_KT, (uint64_t)tSimdException, 0); 

    // It should be interrupt from now on? istrap=0
    SETGATE( idt[T_SYSCALL] ,0,(uint16_t) GD_KT, (uint64_t)tSystemCall, 3);
    //SETGATE( idt[48] ,1,(uint16_t) GD_KT, (uint64_t)tDefault, 0 ); //CBLater: How to set up tDefault?
    SETGATE( idt[IRQ_OFFSET + IRQ_TIMER] ,0,(uint16_t) GD_KT, (uint64_t)tIrqTimer, 0 );
    SETGATE( idt[IRQ_OFFSET + IRQ_KBD] ,0,(uint16_t) GD_KT, (uint64_t)tIrqKbd, 0 );
    SETGATE( idt[IRQ_OFFSET + IRQ_SERIAL] ,0,(uint16_t) GD_KT, (uint64_t)tIrqSerial, 0 );
    SETGATE( idt[IRQ_OFFSET + IRQ_SPURIOUS] ,0,(uint16_t) GD_KT, (uint64_t)tIrqSpurious, 0 );
    SETGATE( idt[IRQ_OFFSET + IRQ_IDE] ,0,(uint16_t) GD_KT, (uint64_t)tIrqIde, 0 );
    SETGATE( idt[IRQ_OFFSET + IRQ_ERROR] ,0,(uint16_t) GD_KT, (uint64_t)tIrqError, 0 );

	// Per-CPU setup
	trap_init_percpu();
}

// Initialize and load the per-CPU TSS and IDT
void
trap_init_percpu(void)
{
	// The example code here sets up the Task State Segment (TSS) and
	// the TSS descriptor for CPU 0. But it is incorrect if we are
	// running on other CPUs because each CPU has its own kernel stack.
	// Fix the code so that it works for all CPUs.
	//
	// Hints:
	//   - The macro "thiscpu" always refers to the current CPU's
	//     struct CpuInfo;
	//   - The ID of the current CPU is given by cpunum() or
	//     thiscpu->cpu_id;
	//   - Use "thiscpu->cpu_ts" as the TSS for the current CPU,
	//     rather than the global "ts" variable;
	//   - Use gdt[(GD_TSS0 >> 3) + 2*i] for CPU i's TSS descriptor;
	//   - You mapped the per-CPU kernel stacks in mem_init_mp()
	//
	// ltr sets a 'busy' flag in the TSS selector, so if you
	// accidentally load the same TSS on more than one CPU, you'll
	// get a triple fault.  If you set up an individual CPU's TSS
	// wrong, you may not get a fault until you try to return from
	// user space on that CPU.
	//
	// LAB 4: Your code here:

	int currCpuNum =  cpunum();
	thiscpu->cpu_ts.ts_esp0 = ( KSTACKTOP - currCpuNum * (KSTKSIZE + KSTKGAP) ); // because, kstacktop_i = KSTACKTOP - i * (KSTKSIZE + KSTKGAP);
	uintptr_t gdtssNCPUAddr = (uintptr_t) (gdt - ((GD_TSS0 >> 3) + 2*currCpuNum));
	//cprintf("\t currCpuNum: %d GD_TSS0: %llx gdt: %llx gdtss0Addr: %llx GD_TSS0 + 16 * currCpuNum: %llx \n", currCpuNum,GD_TSS0,gdt,gdtss0Addr,GD_TSS0 + 16 * currCpuNum);
	//cprintf("\t CPUID: %d gdt+f(): %llx  ((gdt_pd>>16)+40): %llx GD_TSS0 + 16 * currCpuNum: %llx thiscpu->cpu_ts.ts_esp0: %llx \n",currCpuNum,gdt+((GD_TSS0 >> 3) + 2*currCpuNum),((gdt_pd>>16)+40),(GD_TSS0 + 16 * currCpuNum),thiscpu->cpu_ts.ts_esp0);
	SETTSS((struct SystemSegdesc64 *) &gdt[(GD_TSS0 >> 3) + 2*currCpuNum],STS_T64A, (uint64_t) (&thiscpu->cpu_ts),sizeof(struct Taskstate), 0);	

	// Setup a TSS so that we get the right stack
	// when we trap to the kernel.
	//ts.ts_esp0 = KSTACKTOP;
	// Initialize the TSS slot of the gdt.
	//SETTSS((struct SystemSegdesc64 *)((gdt_pd>>16)+40),STS_T64A, (uint64_t) (&ts),sizeof(struct Taskstate), 0);
	// Load the TSS selector (like other segment selectors, the
	// bottom three bits are special; we leave them 0)
	//ltr(GD_TSS0);

	ltr( GD_TSS0 + 16 * currCpuNum); // CBAssumption:this is the offset used in gdtssNCPUAddr. 
	// Load the IDT
	lidt(&idt_pd);
}

void
print_trapframe(struct Trapframe *tf)
{
	cprintf("TRAP frame at %p from CPU %d\n", tf, cpunum());
	//print_regs(&tf->tf_regs);
	cprintf("  es   0x----%04x\n", tf->tf_es);
	cprintf("  ds   0x----%04x\n", tf->tf_ds);
	cprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));
	// If this trap was a page fault that just happened
	// (so %cr2 is meaningful), print the faulting linear address.
	if (tf == last_tf && tf->tf_trapno == T_PGFLT)
		cprintf("  cr2  0x%08x\n", rcr2());
	cprintf("  err  0x%08x", tf->tf_err);
	// For page faults, print decoded fault error code:
	// U/K=fault occurred in user/kernel mode
	// W/R=a write/read caused the fault
	// PR=a protection violation caused the fault (NP=page not present).
	if (tf->tf_trapno == T_PGFLT)
		cprintf(" [%s, %s, %s]\n",
			tf->tf_err & 4 ? "user" : "kernel",
			tf->tf_err & 2 ? "write" : "read",
			tf->tf_err & 1 ? "protection" : "not-present");
	else
		cprintf("\n");
	cprintf("  rip  0x%08x\n", tf->tf_rip);
	cprintf("  cs   0x----%04x\n", tf->tf_cs);
	cprintf("  flag 0x%08x\n", tf->tf_eflags);
	if ((tf->tf_cs & 3) != 0) {
		cprintf("  rsp  0x%08x\n", tf->tf_rsp);
		cprintf("  ss   0x----%04x\n", tf->tf_ss);
	}
}

void
print_regs(struct PushRegs *regs)
{
	cprintf("  r15  0x%08x\n", regs->reg_r15);
	cprintf("  r14  0x%08x\n", regs->reg_r14);
	cprintf("  r13  0x%08x\n", regs->reg_r13);
	cprintf("  r12  0x%08x\n", regs->reg_r12);
	cprintf("  r11  0x%08x\n", regs->reg_r11);
	cprintf("  r10  0x%08x\n", regs->reg_r10);
	cprintf("  r9  0x%08x\n", regs->reg_r9);
	cprintf("  r8  0x%08x\n", regs->reg_r8);
	cprintf("  rdi  0x%08x\n", regs->reg_rdi);
	cprintf("  rsi  0x%08x\n", regs->reg_rsi);
	cprintf("  rbp  0x%08x\n", regs->reg_rbp);
	cprintf("  rbx  0x%08x\n", regs->reg_rbx);
	cprintf("  rdx  0x%08x\n", regs->reg_rdx);
	cprintf("  rcx  0x%08x\n", regs->reg_rcx);
	cprintf("  rax  0x%08x\n", regs->reg_rax);
}
	// Generic system call: pass system call number in AX,
	// up to five parameters in DX, CX, BX, DI, SI.
	// Interrupt kernel with T_SYSCALL.

static void
trap_dispatch(struct Trapframe *tf)
{
	// Handle processor exceptions.
	// LAB 3: Your code here.

	// Handle spurious interrupts
	// The hardware sometimes raises these because of noise on the
	// IRQ line or other reasons. We don't care.
	if (tf->tf_trapno == IRQ_OFFSET + IRQ_SPURIOUS) {
		cprintf("Spurious interrupt on irq 7\n");
		print_trapframe(tf);
		return;
	}

	// Handle clock interrupts. Don't forget to acknowledge the
	// interrupt using lapic_eoi() before calling the scheduler!
	// LAB 4: Your code here.

	// Add time tick increment to clock interrupts.
	// Be careful! In multiprocessors, clock interrupts are
	// triggered on every CPU.
	// LAB 6: Your code here.


	// Handle keyboard and serial interrupts.
	// LAB 5: Your code here.

	// Unexpected trap: The user process or the kernel has a bug.
	//print_trapframe(tf);

	if ( tf->tf_trapno == T_PGFLT )
		page_fault_handler(tf);
	else if ( tf->tf_trapno == T_BRKPT )
		monitor(tf);
	else if(tf->tf_trapno == T_SYSCALL){
		uint64_t x = syscall(tf->tf_regs.reg_rax,tf->tf_regs.reg_rdx,tf->tf_regs.reg_rcx,tf->tf_regs.reg_rbx,tf->tf_regs.reg_rdi,tf->tf_regs.reg_rsi);
		tf->tf_regs.reg_rax = x;
		//asm volatile("movq %0,%%rax" :: "g" (x):"memory");
		return;
	}	
    if(tf->tf_trapno==(IRQ_OFFSET+IRQ_TIMER)){
            lapic_eoi();
	    if ( cpunum() == 0 )
		    time_tick();
        if(tf->tf_cs == GD_KT){
            return;
        }
        lapic_eoi();
        sched_yield(); // Shouldn't return.
        return;
    }
    if(tf->tf_trapno==(IRQ_OFFSET+IRQ_KBD)){
        kbd_intr();
    }
    if(tf->tf_trapno==(IRQ_OFFSET+IRQ_SERIAL)){
        serial_intr();
    }
	if (tf->tf_cs == GD_KT){
		//if(tf->tf_trapno==(IRQ_OFFSET+IRQ_TIMER))
		//	return;
		panic("unhandled trap in kernel from CPU: %d and status: %d \n",cpunum(),thiscpu->cpu_status);
	}
	else {
			env_destroy(curenv);
			return;
	}
}

struct testing1{
	uint64_t a;
	uint64_t b;
	uint64_t c;
};
struct testing{
	struct testing1 temp;
	uint64_t a;
	uint64_t b;
	uint64_t c;
};
void
trap( struct Trapframe *tf )
{
	// The environment may have set DF and some versions
	// of GCC rely on DF being clear
	asm volatile("cld" ::: "cc");

	// Halt the CPU if some other CPU has called panic()
	extern char *panicstr;
	if (panicstr)
		asm volatile("hlt");

	// Re-acqurie the big kernel lock if we were halted in
	// sched_yield()
	if (xchg(&thiscpu->cpu_status, CPU_STARTED) == CPU_HALTED)
		lock_kernel();
	// Check that interrupts are disabled.  If this assertion
	// fails, DO NOT be tempted to fix it by inserting a "cli" in
	// the interrupt path.
	assert(!(read_eflags() & FL_IF));

	//cprintf("Incoming TRAP frame at %p\n", tf);
	if ((tf->tf_cs & 3) == 3) {
		// Trapped from user mode.
		// Acquire the big kernel lock before doing any
		// serious kernel work.
		// LAB 4: Your code here.
		lock_kernel();
		assert(curenv);

		// Garbage collect if current enviroment is a zombie
		if (curenv->env_status == ENV_DYING) {
			env_free(curenv);
			curenv = NULL;
			sched_yield();
		}

		// Copy trap frame (which is currently on the stack)
		// into 'curenv->env_tf', so that running the environment
		// will restart at the trap point.
		curenv->env_tf = *tf;
		// The trapframe on the stack should be ignored from here on.
		tf = &curenv->env_tf;
	}

	// Record that tf is the last real trapframe so
	// print_trapframe can print some additional information.
	last_tf = tf;

	// Dispatch based on what type of trap occurred
	trap_dispatch(tf);

	// If we made it to this point, then no other environment was
	// scheduled, so we should return to the current environment
	// if doing so makes sense.
	if (curenv && curenv->env_status == ENV_RUNNING)
		env_run(curenv);
	else
		sched_yield();
}

void
page_fault_handler(struct Trapframe *tf)
{
    //Faulting Virtual Address
    uint64_t fault_va;

    // Read processor's CR2 register to find the faulting address
    fault_va = rcr2();

    // Handle kernel-mode page faults.
    //How Check whether Kernel Page Faulted?

    // LAB 3: Your code here.
    if((tf->tf_cs & 3)==0){
        cprintf("[%08x] KERNEL fault va %08x ip %08x\n",
        curenv->env_id, fault_va, tf->tf_rip);
        print_trapframe(tf);
        panic("\t Oops page_fault_handler invoked in kernel. va: %llx \n",fault_va);
    } // trapped from kernel mode
        

    // We've already handled kernel-mode exceptions, so if we get here,
    // the page fault happened in user mode.

    // Call the environment's page fault upcall, if one exists.  Set up a
    // page fault stack frame on the user exception stack (below
    // UXSTACKTOP), then branch to curenv->env_pgfault_upcall.
    //
    // The page fault upcall might cause another page fault, in which case
    // we branch to the page fault upcall recursively, pushing another
    // page fault stack frame on top of the user exception stack.
    //
    // The trap handler needs one word of scratch space at the top of the
    // trap-time stack in order to return.  In the non-recursive case, we
    // don't have to worry about this because the top of the regular user
    // stack is free.  In the recursive case, this means we have to leave
    // an extra word between the current top of the exception stack and
    // the new stack frame because the exception stack _is_ the trap-time
    // stack.
    //
    //
    // If there's no page fault upcall, the environment didn't allocate a
    // page for its exception stack or can't write to it, or the exception
    // stack overflows, then destroy the environment that caused the fault.
    // Note that the grade script assumes you will first check for the page
    // fault upcall and print the "user fault va" message below if there is
    // none.  The remaining three checks can be combined into a single test.
    //
    // Hints:
    //   user_mem_assert() and env_run() are useful here.
    //   To change what the user environment runs, modify 'curenv->env_tf'
    //   (the 'tf' variable points at 'curenv->env_tf').

    // LAB 4: Your code here.
    // CBLater: Should also and a condition for stack overflow check.
    if ( !curenv )
	    panic("No curenv please.\n");

    if(!curenv->env_pgfault_upcall){
        cprintf("[%08x] user fault va %08x ip %08x\n",
        curenv->env_id, fault_va, tf->tf_rip);
        print_trapframe(tf);
        env_destroy(curenv);
    }

    //CBLater: Is this za right?
    //CBLater: Stack Overflow ???
    user_mem_assert(curenv, (void*)UXSTACKTOP-PGSIZE, PGSIZE-1, PTE_U|PTE_P|PTE_W);

    // All the check are fine, so prepare trapframe.
    struct UTrapframe prepareUtrapframe;
    uintptr_t currUXSTACKTOP = UXSTACKTOP; // CBLater: Should update it to handle recursive page fault.

    //CBLater: In case of recursive, should first add a zero to stack, how?
    prepareUtrapframe.utf_fault_va = fault_va; // 152(prepareUtrapframe)
    prepareUtrapframe.utf_err = tf->tf_err; // 144((prepareUtrapframe))
    prepareUtrapframe.utf_regs = tf->tf_regs; // 24(prepareUtrapframe)
    prepareUtrapframe.utf_rip = tf->tf_rip; // 16(prepareUtrapframe)
    prepareUtrapframe.utf_eflags = tf->tf_eflags; // 8(prepareUtrapframe)
    prepareUtrapframe.utf_rsp = tf->tf_rsp; // (prepareUtrapframe)

    // Transfer it to UXSTACKTOP.
    if ( tf->tf_rsp <= UXSTACKTOP && tf->tf_rsp > UXSTACKTOP - PGSIZE )
    {
	    currUXSTACKTOP = tf->tf_rsp; //Changing from UXSTACKTOP
	    asm volatile("movq $0,(%%rax)"::"a"(currUXSTACKTOP));
	    currUXSTACKTOP-=8;
    }
    user_mem_assert(curenv,(void*)currUXSTACKTOP-160,160,PTE_U|PTE_P|PTE_W); // UTF is of size 152 bytes.. 
    asm volatile (
        "movq (%%rbx), %%rdx\n"
        "movq %%rdx,-160(%%rax)\n" // Transfer prepareUtrapframe.utf_rsp
        "movq 8(%%rbx), %%rdx\n"
        "movq %%rdx,-152(%%rax)\n" // Transfer prepareUtrapframe.utf_eflags
        "movq 16(%%rbx), %%rdx\n"
        "movq %%rdx,-144(%%rax)\n" // Transfer prepareUtrapframe.utf_rip

        "movq 24(%%rbx), %%rdx\n"
        "movq %%rdx, -136(%%rax)\n" // Transfer prepareUtrapframe.utf_regs.r1
        "movq 32(%%rbx), %%rdx\n"
        "movq %%rdx, -128(%%rax)\n" // Transfer prepareUtrapframe.utf_regs.r2
        "movq 40(%%rbx), %%rdx\n"
        "movq %%rdx, -120(%%rax)\n" // Transfer prepareUtrapframe.utf_regs.r3
        "movq 48(%%rbx), %%rdx\n"
        "movq %%rdx, -112(%%rax)\n" // Transfer prepareUtrapframe.utf_regs.r4

        "movq 56(%%rbx), %%rdx\n"
        "movq %%rdx, -104(%%rax)\n" // Transfer prepareUtrapframe.utf_regs.r5
        "movq 64(%%rbx), %%rdx\n"
        "movq %%rdx, -96(%%rax)\n" // Transfer prepareUtrapframe.utf_regs.r6
        "movq 72(%%rbx), %%rdx\n"
        "movq %%rdx, -88(%%rax)\n" // Transfer prepareUtrapframe.utf_regs.r7
        "movq 80(%%rbx), %%rdx\n"
        "movq %%rdx, -80(%%rax)\n" // Transfer prepareUtrapframe.utf_regs.r8

        "movq 88(%%rbx), %%rdx\n"
        "movq %%rdx, -72(%%rax)\n" // Transfer prepareUtrapframe.utf_regs.r9
        "movq 96(%%rbx), %%rdx\n"
        "movq %%rdx, -64(%%rax)\n" // Transfer prepareUtrapframe.utf_regs.r10
        "movq 104(%%rbx), %%rdx\n"
        "movq %%rdx, -56(%%rax)\n" // Transfer prepareUtrapframe.utf_regs.r11
        "movq 112(%%rbx), %%rdx\n"
        "movq %%rdx, -48(%%rax)\n" // Transfer prepareUtrapframe.utf_regs.r12

        "movq 120(%%rbx), %%rdx\n"
        "movq %%rdx, -40(%%rax)\n" // Transfer prepareUtrapframe.utf_regs.r13
        "movq 128(%%rbx), %%rdx\n"
        "movq %%rdx, -32(%%rax)\n" // Transfer prepareUtrapframe.utf_regs.r14
        "movq 136(%%rbx), %%rdx\n"
        "movq %%rdx, -24(%%rax)\n" // Transfer prepareUtrapframe.utf_regs.r15

        "movq 144(%%rbx), %%rdx\n"
        "movq %%rdx, -16(%%rax)\n" // Transfer prepareUtrapframe.utf_err
        "movq 152(%%rbx), %%rdx\n"
        "movq %%rdx, -8(%%rax)\n" // Transfer prepareUtrapframe.utf_fault_va

        : : "a" (currUXSTACKTOP) , "b" (&prepareUtrapframe): "memory");
    tf->tf_rsp = currUXSTACKTOP - 160;
    tf->tf_rip = (uintptr_t) curenv->env_pgfault_upcall;
    if ( curenv->env_status == ENV_RUNNING )
	    env_run(curenv);
    else{
    // Destroy the environment that caused the fault.
	    cprintf("[%08x] user fault va %08x ip %08x\n",
		    curenv->env_id, fault_va, tf->tf_rip);
	    print_trapframe(tf);
	    env_destroy(curenv);
    }
}
